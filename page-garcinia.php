<?php
/**
 * Template Name: Producto Garcinia
 */
?>

<section class="cover-product cover-garcinia relative bg-faded">
<div class="overlay-nutrition">
  <div class="container">
  <div class="tabla absolute abs-center-x">
    <a href="#" class="close-overlay text-center py-1 text-white fw-400 bg-info rounded-circle float-right mt-4 h6 small absolute">X</a>
    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/nutrition-facts-gr.png" class="img-fluid">
   </div>
  </div>
</div>
<div class="container">
  <div class="row wow fadeInDown" data-wow-delay="0.5s">

    <div class="col-12 my-auto text-white item">
      <div class="row">

        <div class="col-md-6 col-lg-5 col-xl-4 push-lg-7 push-xl-7 push-md-6 mb-5 mb-sm-0">
          <h1 class="fw-300 mb-0 title relative text-success h1">
            esbeltex<br /> <span class="fw-900">Garcinia Cambogia</span>
          </h1>
          <hr class="bg-yellow mt-2">
          <ul class="mb-0 list-inline list-unstlyed icons-products py-4 mb-2 d-flex">
            <li class="list-inline-item pr-4">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/icon-gr1.png" class="img-fluid">
            </li>
            <li class="list-inline-item px-4">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/icon-gr2.png" class="img-fluid">
            </li>
            <li class="list-inline-item px-4">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/icon-gr3.png" class="img-fluid">
            </li>
            <li class="list-inline-item pl-4">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/icon-gr4.png" class="img-fluid">
            </li>
          </ul>

          <div class="hidden-sm-down">
            <p class="text-gray-dark fw-400 small mb-2">Es el complemento que te ayudará a mejorar tu peso, llevando un control en tu salud. Sus componentes principales ayudan a la quema de grasa y controlan la ingesta de alimentos.
            </p>
            <p class="text-gray-dark fw-400 small">“Una buena alimentación y ejercicio te ayudará a obtener mejores resultados”.</p>
            <a href="#" class="btn btn-success rounded btn-sm px-3 text-white nutrition-facts"> Nutrition Facts</a>
          </div>
        </div>

        <div class="col-md-5 col-lg-4 col-xl-3 pull-md-5 pull-lg-2 pull-xl-0 my-auto text-center">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/garcinia.png" class="img-fluid">
           <div class="hidden-md-up texto-product">
            <p class="text-gray-dark fw-400 small mb-2">Es el complemento que te ayudará a mejorar tu peso, llevando un control en tu salud. Sus componentes principales ayudan a la quema de grasa y controlan la ingesta de alimentos.
            </p>
            <p class="text-gray-dark fw-400 small">“Una buena alimentación y ejercicio te ayudará a obtener mejores resultados”.</p>
            <a href="#" class="btn btn-success rounded btn-sm px-3 text-white nutrition-facts"> Nutrition Facts</a>
          </div>
        </div>

      </div>
    </div>


  </div>
</div>

</section>


<section class="benefits-products py-6 skew-bg">
  <div class="container my-0 my-md-6">
   <img src="<?php echo get_template_directory_uri(); ?>/assets/images/circle-gr.png" class="text-bg abs-center img-fluid wow fadeInUp hidden-xs-down">
  <div class="row">
    <div class="col-sm-6 offset-sm-3 text-center text-white py-2">
      <h2 class="title h2 text-yellow wow fadeInUp" data-wow-delay="0.3s">
        esbeltex <br />
garcinia cambogia
      </h2>
      <p class="mb-0 lead wow fadeInUp" data-wow-delay="0.8s">El complemento perfecto 100% natural para lograr la talla correcta. </p>
    </div>
  </div>
  </div>
</section>


<section class="ingredientes py-2">
  <div class="container">



  <div class="section-title wow slideInUp mx-auto text-center mb-5">
    <h2 class="h1 red text-uppercase text-green fw-900 mt-0">INGREDIENTES</h2>
  <p>La garcinia es un fruto que tiene distintas funciones, esbeltex contiene extracto de esta fruta estimulando la quema de grasa y controlan la ingesta de alimentos, gracias al conjunto de ingredientes que contiene el producto.</p>
  <hr class="divider bg-green rounded mx-auto">
  </div>


  <div class="text-center py-4">
    <ul class="icons-products list-inline list-unstlyed d-inline-flex mb-5 wow fadeInUp">
      <li class="list-inline-item align-self-xl-stretch px-3 px-md-6">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/icon-grb2.png" class="img-fluid">
      <div class="clearfix"></div>
      <p class="py-3 wow fadeInUp small">
      Acido hidroxicítrico actúa sobre la quema de grasas.
      </p>
      </li>
      <li class="list-inline-item align-self-xl-stretch px-3 px-md-6">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/icon-grb3.png" class="img-fluid">
      <div class="clearfix"></div>
      <p class="py-3 wow fadeInUp small">
      Al absorberse, evita la síntesis de grasas en el cuerpo.
      </p>
      </li>
      <li class="list-inline-item align-self-xl-stretch px-3 px-md-6">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/icon-grb4.png" class="img-fluid">
      <div class="clearfix"></div>
      <p class="py-3 wow fadeInUp small">
      Ayuda a elevar la temperatura del cuerpo utilizando más energía y quemando grasa.
      </p>
      </li>
      <li class="list-inline-item align-self-xl-stretch px-3 px-md-6">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/icon-grb1.png" class="img-fluid">
      <div class="clearfix"></div>
      <p class="py-3 wow fadeInUp small">
      Proporciona antioxidantes y ayuda a que el cromo se absorba para realizar sus funciones.
      </p>
      </li>
    </ul>
  </div>

  </div>
</section>


<section class="beneficios bg-faded">

<div class="container">

<div class="section-title wow slideInUp mx-auto text-center mb-5">
    <h2 class="h1 red text-uppercase text-orange fw-900 pt-3 wow fadeInUp">Beneficios</h2>
  <p class="wow fadeInUp">Son muchos los beneficios para tu salud. <br />
Hoy puedes empezar a tener una nueva calidad de vida.</p>
  <hr class="divider bg-orange rounded mx-auto">
  </div>


  <div class="clearfix">
    <div class="col-xs-12">

    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/gr-girl-beneficios.png" class="img-fluid absolute woman-beneficios">

    <div class="hidden-md-up text-center px-4">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/limon.png" class="img-fluid text-center px-5">

      <h4 class="h4 fw-400 text-orange wow fadeInUp bottom-text lh-12 my-6">Los componentes de esbeltex weight control son la fórmula perfecta ya que actúan de manera conjunta, te ayudarán a que tu proceso de disminución de peso sea más efectivo, seguro y experimentes una mejor calidad de vida.</h4>

      <span class="sep-dashed text-green"></span>


      <h4 class="text-green"> ● Acelera el metabolismo </h4> <p class="fw-300 h5 lh-14 mb-6"> La garcinia cambogia apoya al metabolismo a quemar el exceso de grasa del cuerpo.</p>

      <span class="sep-dashed text-green"></span>
      <h4 class="text-green"> ● Sin efectos secundarios. </h4>
      <p class="fw-300 h5 lh-14 mb-6">  </p>

      <span class="sep-dashed text-green"></span>
      <h4 class="text-green"> ● SafeProduct </h4> <p class="fw-300 h5 lh-14 mb-6"> Son muy pocas las contraindicaciones para el uso de esbeltex</p>

      <span class="sep-dashed text-green"></span>
      <h4 class="text-green"> ● 100% natural </h4> <p class="fw-300 h5 lh-14 mb-6"> todos los componentes de esbeltex son de origen natural.</p>

      <span class="sep-dashed text-green"></span>
      <h4 class="text-green"> ● Resultados comprobados </h4> <p class="fw-300 h5 lh-14 mb-6"> únete a la experiencia esbeltex y prueba los resultados.</p>

      <span class="sep-dashed text-green"></span>
      <h4 class="text-green"> ● Disminuye el apetito </h4> <p class="fw-300 h5 lh-14 mb-6"> debido a la cantidad de fibra que proporciona la garcinia, te ayudará a causar saciedad evitando que excedas tu consumo de alimentos.</p>
<span class="sep-dashed text-green"></span>
      <h4 class="text-green">● 100% natural </h4>
<p class="fw-300 h5 lh-14 mb-6">todos los componentes de esbeltex son de origen 100% natural.</p>

    </div>

<img src="<?php echo get_template_directory_uri(); ?>/assets/images/beneficios-garcinia.png" class="img-fluid mx-auto d-block wow zoomIn hidden-sm-down" data-wow-delay="0.5s">

<div class="row">
  <div class="col-md-6 push-md-3 col-lg-4 push-lg-4 text-center py-6">
    <p class="h4 fw-400 text-green wow fadeInUp bottom-text lh-14">Adquiere hoy esbeltex garcinia cambogia, controla tu peso y recupera el control de tu salud. Vuelve a la talla que es con esbeltex.</p>
<a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=10" class="btn text-white bg-orange rounded px-3 mb-6 mt-6 wow fadeInUp">Donde Comprar</a>
  </div>
</div>
    </div>
  </div>
</div>

</section>


<section class="featured py-2 bg-green">

<div class="container">

  <div class="text-center">
    <ul class="mb-0 list-inline list-unstlyed py-3 d-sm-inline-flex d-block wow fadeInUp">
            <li class="d-block d-sm-inline-block px-2 px-lg-4 align-middle">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/cl-feat-1.png" class="img-fluid mb-1 ml-5 ml-sm-0">
              <p class="small text-white mr-sm-5">esbeltex contiene menos <br /> 20 ppm de gluten
</p>
            </li>
            <li class="d-block d-sm-inline-block px-2 px-lg-4 align-middle">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/cl-feat-2.png" class="img-fluid mb-1">
               <p class="small text-white">esbeltex no ha sido modificado<br /> genéticamente para alterar<br /> su propiedades
</p>

          </ul>
  </div>

  <div class="row">
    <div class="push-sm-2 col-sm-8">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/O6u7yI1hphI?rel=0&modestbranding=1&autohide=1&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>
  </div>

</div>

</section>

<section class="reviews-wrapper py-6">

<div class="container">

  <div class="row">
    <div class="col-10 push-1">
      <div id="accordion" role="tablist" aria-multiselectable="true">


 <?php
 global $post;
 $i=0;
      $loop = new WP_Query( array(
        'showposts' => 100,
        'post_type' => array('reviews'),
        'orderby' => 'rand',
        'tax_query' => array(
            array(
              'taxonomy' => 'custom_cat_review',
              'field'    => 'slug',
              'terms'    => 'garcinia',
            ),
          )
        )
      );
    while ( $loop->have_posts() ) : $loop->the_post(); ?>

    <?php $rating = get_field('rating'); ?>

   <div class="card mb-3" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $post->ID ?>" aria-expanded="true" aria-controls="collapse-<?php echo $post->ID ?>">
    <div class="card-header" role="tab" id="heading-<?php echo $post->ID ?>">
      <h5 class="mb-0">
          <div class="row">
            <div class="col-2 bg-rating hidden-xs-down">
              <div class="px-md-2 py-4 mt-2">
               <?php if ( has_post_thumbnail() ) : ?>
                  <?php the_post_thumbnail('thumbnail', ['class' => 'img-fluid rounded-circle']); ?>
                  <?php else : ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/avatar-rv.png" class="img-fluid">
                <?php endif; ?>
              </div>
            </div>
            <div class="col-12 col-sm-10 bg-rating">
              <div class="px-2 py-4 mb-2 text-white">
              <ul class="rating rating-<?php echo $rating; ?> list-inline list-unstyled">
              <li class="star list-inline-item">1</li>
              <li class="star list-inline-item">2</li>
              <li class="star list-inline-item">3</li>
              <li class="star list-inline-item">4</li>
              <li class="star list-inline-item">5</li>
            </ul>
               <h3 class="h4 fw-900">“<?php the_title(); ?>”</h3>
               <?php if(get_field('extracto')) { ?>
                 <p class="mb-1 h6"> <?php the_field('extracto') ?> </p>
               <?php } ?>
               <?php if(get_field('autor')) { ?>
                 <p class="mb-0 h6"> <?php the_field('autor') ?> </p>
               <?php } ?>
            </div>
          </div>
        </div>
      </h5>
    </div>
    <div id="collapse-<?php echo $post->ID ?>" class="collapse <?php echo ($i==0)?'show':''; ?>" role="tabpanel" aria-labelledby="heading-<?php echo $post->ID ?>">
      <div class="card-block">
        <div class="row">
          <div class="push-sm-2 col-sm-10">
            <div class="py-4 px-sm-2">
              <?php the_content(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

   <?php  $i++; endwhile; ?>
   <?php wp_reset_query(); ?>


      </div>
    </div>
  </div>
</div>

</section>
