<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning mb-0">
        <?php _e('Estas utilizando un navegador <strong>antiguo</strong>. Por favor <a href="http://browsehappy.com/">actualiza tu navegador</a> para mejorar tu experiencia.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <div class="wrap" role="document">
    <div class="container py-6">

    <div class="page-header row text-center text-info">
    <h1 class="fw-300 mx-auto h2 mt-4 wow fadeInUp px-3">
      ¡Encuentra aquí tips, noticias, promociones <br class="hidden-sm-down"/>
      y consejos que te ayudarán a complementar <br class="hidden-sm-down"/>
      <span class="fw-900">tu nuevo estilo de vida con esbeltex!</span>
    </h1>
  </div>

    <div class="text-center categories-list clearfix mx-auto my-6 wow fadeInUp" data-wow-delay="0.3s">

    </div>


      <div class="content row">
        <main class="main">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <aside class="sidebar">
          <?php if (Setup\display_sidebar()) : ?>
            <?php include Wrapper\sidebar_path(); ?>
          <?php endif; ?>
          <a href="#" class="show-panel">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/promo-esbeltex.png" class="img-fluid">
          </a>
        </aside><!-- /.sidebar -->
      </div><!-- /.content -->
    </div>
    </div><!-- /.wrap -->
    <?php get_template_part('templates/newsletter'); ?>
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
