<div class="container py-6">


  <div class="page-header row text-center text-info">
    <h1 class="fw-300 mx-auto h2 mt-4 wow fadeInUp px-3">
      ¡Encuentra aquí tips, noticias, promociones <br class="hidden-sm-down"/>
      y consejos que te ayudarán a complementar <br class="hidden-sm-down"/>
      <span class="fw-900">tu nuevo estilo de vida con esbeltex!</span>
    </h1>
  </div>

    <div class="text-center categories-list clearfix mx-auto my-5 wow fadeInUp" data-wow-delay="0.3s">


<ul class="categories-list">
    <?php wp_list_categories('show_option_all=&hide_empty=1&title_li='); ?>
</ul>

    </div>

  <div class="row">
    <div class="col-sm-8">
      <?php if (!have_posts()) : ?>
        <div class="alert alert-warning">
          <?php _e('Sorry, no results were found.', 'sage'); ?>
        </div>
        <?php get_search_form(); ?>
      <?php endif; ?>

      <div class="row">
        <?php while (have_posts()) : the_post(); ?>
          <div class="col-sm-6">
            <article <?php post_class('text-center'); ?>>
            <div class="img-wrapper">
              <a href="<?php the_permalink(); ?>">
                <?php if ( has_post_thumbnail() ) {
                the_post_thumbnail('post-img-blog', array( 'class'  => 'img-fluid mb-3' ));
                } else { ?>
                <img class="img-fluid mb-3" src="<?php bloginfo('template_directory'); ?>/assets/images/no-image-post-home.png"
                alt="<?php the_title(); ?>" />
                <?php } ?>
              </a>
            </div>
              <header>
                <h2 class="entry-title h5 fw-700 lh-12"><a href="<?php the_permalink(); ?>" class="text-muted"><?php the_title(); ?></a></h2>
              </header>
              <div class="entry-summary small px-2">
                <?php
                $content = get_the_content();
                echo wp_trim_words( $content , '15' ); ?>
              </div>
              <footer>
                <ul class="list-unstyled list-inline py-4 text-center">
                  <li class="list-inline-item px-3 align-top">
                    <a href="<?php the_permalink(); ?>" class="btn btn-blog btn-warning px-3 py-0 rounded">Ver más</a>
                  </li>
                  <li class="list-inline-item mr-2 align-middle"> <a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><i class="fa fa-2x text-info fa-facebook-square" aria-hidden="true"></i></a> </li>
                  <li class="list-inline-item mr-2 align-middle"> <a target="_blank" href="http://twitter.com/share?text=<?php the_title(); ?>&amp;url=<?php the_permalink(); ?>"><i class="fa fa-2x text-info fa-twitter-square" aria-hidden="true"></i></a> </li>
                </ul>
              </footer>
            </article>
          </div>
        <?php endwhile; ?>
      </div>

      <?php bones_page_navi(); ?>
    </div>

    <div class="col-12 col-sm-4">
      <?php get_template_part('templates/sidebar'); ?>
      <a href="#" class="show-panel">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/promo-esbeltex.png" class="img-fluid">
      </a>
    </div>
  </div>

</div>
