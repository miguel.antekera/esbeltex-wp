function drawInfobox(category, infoboxContent, json, i){

    var ibContent = '';
    ibContent =
    '<div class="infobox">' +
        '<div class="inner">' +
            '<div class="image">' +
                    '<div class="meta">' +
                        '<h5 class="title">' + json[i].name  +  '</h5>' +
                        '<span><span class="fw-700">Dirección:</span> ' + json[i].map_location.address  +  '</span> <br>' +
                        '<span><span class="fw-700">Productos:</span> ' + json[i].products  +  ' </span>' +
                    '</div>' +
                '</a>' +
            '</div>' +
        '</div>' +
    '</div>';

    return ibContent;
}
