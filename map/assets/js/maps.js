"use strict";
var $ = jQuery.noConflict();
var myUrl = window.location.origin+'/';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// jQuery
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var mapStyles = [ {"featureType":"road","elementType":"labels","stylers":[{"visibility":"simplified"},{"lightness":20}]},{"featureType":"administrative.land_parcel","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"transit","elementType":"all","stylers":[{"saturation":-100},{"visibility":"on"},{"lightness":10}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":50}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#a1cdfc"},{"saturation":30},{"lightness":49}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"hue":"#f49935"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"hue":"#fad959"}]}, {featureType:'road.highway',elementType:'all',stylers:[{hue:'#dddbd7'},{saturation:-92},{lightness:60},{visibility:'on'}]}, {featureType:'landscape.natural',elementType:'all',stylers:[{hue:'#c8c6c3'},{saturation:-71},{lightness:-18},{visibility:'on'}]},  {featureType:'poi',elementType:'all',stylers:[{hue:'#d9d5cd'},{saturation:-70},{lightness:20},{visibility:'on'}]} ];
var $ = jQuery.noConflict();
$(document).ready(function($) {
    "use strict";

    // Bootstrap tooltip

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    // reset input
    $('.reset-search').on('click', function() {
      $('.deletable').val('');
      $(this).fadeOut();
      event.preventDefault();
    });

    $('.deletable').on('keyup', function(){
       var text_value=$(this).val();
       if(text_value!='') {
          $('.reset').fadeIn('fast').css("display","inline-block");
        } else {
          $('.reset').fadeOut('fast');
        }
     })
});

// Autocomplete address ------------------------------------------------------------------------------------------------

function autoComplete(){
    if( !$("script[src='"+myUrl+"/wp-content/themes/esbeltex/map/assets/js/leaflet.js']").length ){
        var input = document.getElementById('location') ;
        var autocomplete = new google.maps.places.Autocomplete(input, {
            types: ["geocode"]
        });
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                return;
            }

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }
        });

    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Homepage map - Google
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function createHomepageGoogleMap(_latitude,_longitude,json){

    $.get(myUrl+"/wp-content/themes/esbeltex/map/assets/external/_infobox.js", function() {
        gMap();
    });
    function gMap(){
        var mapCenter = new google.maps.LatLng(_latitude,_longitude);
        var mapOptions = {
            zoom: 12,
            center: mapCenter,
            disableDefaultUI: false,
            scrollwheel: false,
            styles: mapStyles,
            clickableIcons: false,

            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.BOTTOM_CENTER
            },
            panControl: false,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.RIGHT_TOP
            }
        };
        var mapElement = document.getElementById('map');
        var map = new google.maps.Map(mapElement, mapOptions);
        var newMarkers = [];
        var markerClicked = 0;
        var activeMarker = false;
        var lastClicked = false;

        for (var i = 0; i < json.length; i++) {


            // Google map marker content -----------------------------------------------------------------------------------

            var markerContent = document.createElement('DIV');
                markerContent.innerHTML =
                    '<div class="map-marker marker-'+json[i].id+'">' +
                        '<div class="icon">' +
                        '<img src="'+myUrl+'/wp-content/themes/esbeltex/assets/images/icons/location.png">' +
                        '</div>' +
                    '</div>';

            // Create marker on the map ------------------------------------------------------------------------------------

            var marker = new RichMarker({
                position: new google.maps.LatLng( json[i].lat, json[i].long ),
                map: map,
                draggable: false,
                content: markerContent,
                flat: true
            });

            newMarkers.push(marker);

            // Create infobox for marker -----------------------------------------------------------------------------------

            var infoboxContent = document.createElement("div");
            var infoboxOptions = {
                content: infoboxContent,
                disableAutoPan: false,
                pixelOffset: new google.maps.Size(-18, -42),
                zIndex: null,
                alignBottom: true,
                boxClass: "infobox",
                enableEventPropagation: true,
                closeBoxMargin: "0px 0px -30px 0px",
                closeBoxURL: ""+myUrl+"/wp-content/themes/esbeltex/map/assets/img/close.png",
                infoBoxClearance: new google.maps.Size(1, 1)
            };

            // Infobox HTML element ----------------------------------------------------------------------------------------

            var category = json[i].category;
            infoboxContent.innerHTML = drawInfobox(category, infoboxContent, json, i);

            // Create new markers ------------------------------------------------------------------------------------------

            newMarkers[i].infobox = new InfoBox(infoboxOptions);

            // Show infobox after click ------------------------------------------------------------------------------------

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    google.maps.event.addListener(map, 'click', function(event) {
                        lastClicked = newMarkers[i];
                    });
                    activeMarker = newMarkers[i];
                    if( activeMarker != lastClicked ){
                        for (var h = 0; h < newMarkers.length; h++) {
                            newMarkers[h].content.className = 'marker-loaded';
                            newMarkers[h].infobox.close();
                        }
                        newMarkers[i].infobox.open(map, this);
                        newMarkers[i].infobox.setOptions({ boxClass:'fade-in-marker'});
                        newMarkers[i].content.className = 'marker-active marker-loaded';
                        markerClicked = 1;
                    }
                }
            })(marker, i));

            // Fade infobox after close is clicked -------------------------------------------------------------------------

            google.maps.event.addListener(newMarkers[i].infobox, 'closeclick', (function(marker, i) {
                return function() {
                    activeMarker = 0;
                    newMarkers[i].content.className = 'marker-loaded';
                    newMarkers[i].infobox.setOptions({ boxClass:'fade-out-marker' });
                }
            })(marker, i));
        }

        // Close infobox after click on map --------------------------------------------------------------------------------

        google.maps.event.addListener(map, 'click', function(event) {
            if( activeMarker != false && lastClicked != false ){
                if( markerClicked == 1 ){
                    activeMarker.infobox.open(map);
                    activeMarker.infobox.setOptions({ boxClass:'fade-in-marker'});
                    activeMarker.content.className = 'marker-active marker-loaded';
                }
                else {
                    markerClicked = 0;
                    activeMarker.infobox.setOptions({ boxClass:'fade-out-marker' });
                    activeMarker.content.className = 'marker-loaded';
                    setTimeout(function() {
                        activeMarker.infobox.close();
                    }, 350);
                }
                markerClicked = 0;
            }
            if( activeMarker != false ){
                google.maps.event.addListener(activeMarker, 'click', function(event) {
                    markerClicked = 1;
                });
            }
            markerClicked = 0;
        });

        // Create marker clusterer -----------------------------------------------------------------------------------------

        var clusterStyles = [
            {
                url: myUrl+'/wp-content/themes/esbeltex/map/assets/img/cluster.png',
                height: 34,
                width: 34
            }
        ];

        var markerCluster = new MarkerClusterer(map, newMarkers, { styles: clusterStyles, maxZoom: 19 });
        markerCluster.onClick = function(clickedClusterIcon, sameLatitude, sameLongitude) {
            return multiChoice(sameLatitude, sameLongitude, json);
        };

        // Dynamic loading markers and data from JSON ----------------------------------------------------------------------

        google.maps.event.addListener(map, 'idle', function() {
            var visibleArray = [];
            for (var i = 0; i < json.length; i++) {
                if ( map.getBounds().contains(newMarkers[i].getPosition()) ){
                    visibleArray.push(newMarkers[i]);
                    $.each( visibleArray, function (i) {
                        setTimeout(function(){
                            if ( map.getBounds().contains(visibleArray[i].getPosition()) ){
                                if( !visibleArray[i].content.className ){
                                    visibleArray[i].setMap(map);
                                    visibleArray[i].content.className += 'bounce-animation marker-loaded';
                                    markerCluster.repaint();
                                }
                            }
                        }, i * 50);
                    });
                } else {
                    newMarkers[i].content.className = '';
                    newMarkers[i].setMap(null);
                }
            }

            var visibleItemsArray = [];
            $.each(json, function(a) {
                if( map.getBounds().contains( new google.maps.LatLng( json[a].lat, json[a].long ) ) ) {
                    var category = json[a].category;
                    pushItemsToArray(json, a, category, visibleItemsArray);
                }
            });

            // If array is empty show message ---------------------------------------------------------------------

            if (visibleItemsArray.length === 0) {
              $('.items-list .results').html("<p class='h6'> No se encontraron tiendas en la zona.</h6>");
            }
            else {
              $('.items-list .results').html( visibleItemsArray );
            }

            // Hover items in map ---------------------------------------------------------------------

            var $singleItem = $('.results .item');

            $($singleItem).on("mouseenter", function() {
              var id = $(this).attr('id');
              $('.marker-'+id).addClass('marker-active');
            });

            $($singleItem).on("mouseleave", function() {
              var id = $(this).attr('id');
              $('.marker-'+id).removeClass('marker-active');
            });

        });


        redrawMap('google', map);

        // Geolocation of user -----------------------------------------------------------------------------------------

        $('.geolocation').on("click", function() {
          $(this).addClass('loading');

            if (navigator.geolocation) {
               navigator.geolocation.getCurrentPosition(function(position) {

           var _latitude = position.coords.latitude;
           var _longitude = position.coords.longitude;

           $('#map').empty();
           scrollToMap();

           initialize(_latitude, _longitude);

         }, function() {
             console.log("Geo Location is not supported");
             $('.geolocation').removeClass('loading');
         });

            } else {
                alert('Geo Location is not supported');
                $('.geolocation').removeClass('loading');
            }
        });



     //    if (navigator.geolocation) {

     //     navigator.geolocation.getCurrentPosition(function(position) {

     //       var _latitude = position.coords.latitude;
     //       var _longitude = position.coords.longitude;

     //       scrollToMap();

     //       initialize(_latitude, _longitude);

     //     }, function() {
     //        initialize(_latitude, _longitude);
     //         handleLocationError(true, infoWindow, map.getCenter());
     //         console.log("No location");
     //     });
     //     // end if geolocation
     // } else {
     //      handleLocationError(false, infoWindow, map.getCenter());
     //     console.log("Browser doesn't support Geolocation");

     // }


        function success(position) {
            var locationCenter = new google.maps.LatLng( position.coords.lat, position.coords.long);
            map.setCenter( locationCenter );
            map.setZoom(12);

      var markerContent = document.createElement('DIV');
      markerContent.innerHTML =
        '<div class="map-marker">' +
          '<div class="icon">' +
          '</div>' +
        '</div>';

      // Create marker on the map ------------------------------------------------------------------------------------

      var marker = new RichMarker({
        position: locationCenter,
        map: map,
        draggable: false,
        content: markerContent,
        flat: true
      });

      marker.content.className = 'marker-loaded';

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                "latLng": locationCenter
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var lat = results[0].geometry.location.lat(),
                        lng = results[0].geometry.location.lng(),
                        placeName = results[0].address_components[0].long_name,
                        latlng = new google.maps.LatLng(lat, lng);

                    $("#location").val(results[0].formatted_address);
                }
            });

        }


        // Autocomplete address ----------------------------------------------------------------------------------------

        var input = document.getElementById('location') ;
        var autocomplete = new google.maps.places.Autocomplete(input, {
            types: ["geocode"]
        });
        autocomplete.bindTo('bounds', map);
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();


            if (!place.geometry) {
                return;
            }
            if (place.geometry.viewport) {
                scrollToMap();

                map.fitBounds(place.geometry.viewport);
                map.setZoom(12);
                google.maps.event.trigger(map, 'resize');

            } else {
                scrollToMap();
                map.setCenter(place.geometry.location);
                map.setZoom(12);
                google.maps.event.trigger(map, 'resize');
            }

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }
        });


    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Push items to array and create <li> element in Results sidebar ------------------------------------------------------

function pushItemsToArray(json, a, category, visibleItemsArray){
    visibleItemsArray.push(
        '<li>' +
            '<div class="item" id="' + json[a].id + '">' +
                '<div class="wrapper">' +
                    '<h5 class="h6 fw-700 mb-1">' + json[a].name + '</h5>' +
                    '<address class="mb-0 small">Dirección: ' + json[a].map_location.address + '</address>' +
                    '<div class="info">' +
                        '<span class="fw-400 small">Productos: ' + json[a].products + '</span> <br />' +
                        // '<span class="fw-400 small"><a href="tel:'+json[a].acf.telefono+'">'+json[a].acf.telefono+'</a></span>' +
                        // ' - <span class="fw-400 small"><a href="http://'+json[a].acf.website+'">'+json[a].acf.website+'</a></span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</li>'
    );
}

// Center map to marker position if function is called (disabled) ------------------------------------------------------

function centerMapToMarker(){
    $.each(json, function(a) {
        if( json[a].id == id ) {
            var _latitude = json[a].lat;
            var _longitude = json[a].long;
            var mapCenter = new google.maps.LatLng(_latitude,_longitude);
            map.setCenter(mapCenter);
        }
    });
}

// Redraw map after item list is closed --------------------------------------------------------------------------------

function redrawMap(mapProvider, map){
    $('.toggle-navigation').click(function() {
        $('.map-canvas').toggleClass('results-collapsed');
        $('.map-canvas .map').one("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){
            if( mapProvider == 'osm' ){
                map.invalidateSize();
            }
            else if( mapProvider == 'google' ){
                google.maps.event.trigger(map, 'resize');
            }
        });
    });
}

// Scroll to map --------------------------------------------------------------------------------

function scrollToMap () {
  var height = $('#map').height();

  $('.items-list .results').empty();

  $('html,body').animate({
      scrollTop: $('.map-canvas').show().offset().top - 150},
      'slow');

  if (window.matchMedia("(min-width: 768px)").matches) {
    $('.map-canvas').css({"visibility": "visible"}).animate({height: height},700);
  } else {
    $('.map-canvas').css({"visibility": "visible", "height": "auto"});
  }
}
