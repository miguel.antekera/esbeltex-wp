<?php
/**
 * Template Name: Porque esbeltex
 */
?>

<section class="cover-porque cover-general relative text-center">
  <div class="container pt-0 pt-md-4 cover-porque-top">
    <div class="row  relative">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/porque_cover_1.png" class="img-fluid absolute girl wow fadeInUp" data-wow-delay="0.8s">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/porque_cover_2.png" class="img-fluid absolute fruits wow fadeInUp hidden-sm-down" data-wow-delay="0.8s">
      <div class="col-sm-8 push-sm-2 wow fadeInDown" data-wow-delay="0.5s">
        <h1 class="fw-300 mb-2 title relative text-danger h4 mt-0 mt-md-6">
        No esperes a que te digan <br  class="hidden-sm-down"/>
        que debes tener <br  class="hidden-sm-down"/>
        <span class="fw-900">una vida saludable. </span> <br  class="hidden-sm-down"/>
        Disminuye tu peso y mejora <br  class="hidden-sm-down"/>
        tu control de salud.
        </h1>
        <h2 class="subtitle h4 pt-3 pb-6 mb-4">
          Con esbeltex tú puedes mejorar <br  class="hidden-sm-down"/>
          y  tener un mayor control de tu salud <br  class="hidden-sm-down"/>
          mientras pierdes peso.
        </h2>
      </div>
    </div>
  </div>
</section>

<section class="cover-porque-bottom relative bg-danger text-white pb-6 skew-top-right text-center">
  <div class="container pt-3">
    <h3 class="lead lh-14 wow fadeInUp">
      <span class="fw-400">esbeltex Garcinia Cambogia y esbeltex Colon Cleanse Detox</span> <br  />
      son los complementos perfectos por los muchos beneficios que te<br  class="hidden-sm-down"/>
      ofrecen para ayudarte a tener un estilo de vida más saludable en donde<br  class="hidden-sm-down"/>
      eres tú quien tiene el control y el manejo del de tu peso.
    </h3>
  </div>
</section>

<section class="bottom-content-porque">

  <div class="container">
    <div class="row">
      <div class="col-md-5 push-md-1 text-right col-left">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/porque_joined1.png" class="icon absolute wow fadeInUp">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/garcinia.png" class="img-fluid product px-sm-3 wow fadeInLeft" data-wow-delay="0.5s">
        <div class="content px-5 py-3 wow fadeInUp">
          <h4 class="text-success h2 fw-700">
            esbeltex<br />
            Garcinia Cambogia
          </h4>
          <p class="mb-4">
            Esbeltex Garcinia es el producto ideal como complemento en tu tratamiento para la disminución de peso. Es un producto seguro al tener muy pocas contra indicaciones y sin efectos secundarios. Elige un producto que te asegure resultados, esbeltex te ayudará a lograrlo.
          </p>
          <ul class="list-unstyled text-success">
            <li> 100% natural ●</li>
            <li>Acelera el metabolismo ●</li>
            <li>Quema grasa ●</li>
            <li>Estimula el consumo de energía ●</li>
            <li>Ayuda a mantener la masa muscular ●</li>
          </ul>
        </div>
      </div>
      <div class="col-md-5 push-md-1 col-right">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/porque_joined2.png" class="icon absolute wow fadeInUp">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/colon.png" class="img-fluid product px-sm-3 wow fadeInRight" data-wow-delay="0.5s">
        <div class="content px-5 py-3 wow fadeInUp">
          <h4 class="text-blue-dark h2 fw-700">
            esbeltex Colon<br />
            Cleanse Detox
          </h4>
          <p class="mb-4">
            Mantener un colon saludable te ayudará a que los demás órganos se mantengan saludables, evitando el acumulo de toxinas en el organismo. Un producto 100% natural que te ayudará a sentirte mejor Con esbeltex Colon Cleanse puedes lograr un abdomen plano y una mejor salud intestinal.
          </p>
          <ul class="list-unstyled text-blue-dark">
            <li>● 100% natural</li>
            <li>● Mejora la digestión</li>
            <li>● Produce Ligereza</li>
            <li>● Previene el estreñimiento</li>
          </ul>
        </div>
      </div>
    </div>

    <div class="text-center">
      <ul class="icons-plus list-inline list-unstlyed d-inline-flex mb-5 py-6 wow fadeInUp wow fadeInUp">
        <li class="list-inline-item px-3 px-md-5">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/porque_icon_1.png" class="img-fluid mb-3">
          <h6>Alimentación</h6>
        </li>
        <li class="list-inline-item px-3 px-md-5">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/porque_icon_2.png" class="img-fluid mb-3">
          <h6>Dieta</h6>
        </li>
        <li class="list-inline-item px-3 px-md-5">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/porque_icon_3.png" class="img-fluid mb-3">
          <h6>esbeltex</h6>
        </li>
        <li class="list-inline-item px-3 px-md-5">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/porque_icon_4.png" class="img-fluid mb-3">
          <h6>Ejercicio</h6>
        </li>
      </ul>
    </div>

    <div class="row wow fadeInUp hidden-sm-down">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo_porque.png" class="img-fluid mx-auto mb-6 pb-5">
    </div>

  </div>

</section>
