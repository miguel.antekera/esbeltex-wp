<?php
/**
 * Template Name: Contacto
 */
?>

<section class="bg-faded">
  <div class="page-header text-center pt-6 pb-5 px-4">
    <h1 class="fw-300 mx-auto h2 mb-3 mt-4 wow fadeInUp title text-primary">
      Estamos listos para ayudarte
    </h1>
    <p class="pb-4 wow fadeInUp">Si tienes preguntas, sugerencias sobre cualquiera de nuestros productos esbeltex <br class="hidden-sm-down"> Garcinia Cambogia o  esbeltex Colon Cleanse Detox o si quieres conocer las <br class="hidden-sm-down"> últimas noticias sobre cómo mantener una vida saludable,  contáctanos</p>
  </div>
</section>

<section class="bg-contact">
  <div class="container">
    <div class="row">
      <div class="col-md-5 push-md-1 bg-contact-image hidden-sm-down">

      </div>
      <div class="col-md-5 push-md-1">
        <div class="px-4 py-5">
          <?php while (have_posts()) : the_post(); ?>
            <?php get_template_part('templates/content', 'page'); ?>
          <?php endwhile; ?>
          <br /><br /><br /><br /><br />
        </div>
      </div>
    </div>
  </div>
</section>

<section class="bottom-contact bg-faded skew-top-right pt-6">
  <div class="container">
    <div class="row">
      <div class="col-md-5 push-md-6 text-right wow fadeInUp">
        <p class="my-0 text-gray-dark tw-msg">
          Si necesitas ayuda, quieres hacer una devolución o tienes alguna duda sobre cualquiera de los productos esbeltex, escríbenos <a href="https://twitter.com/esbeltex" target="_blank">@esbeltex</a> en twitter. </p>
          <span class="icon-tw ">
            <a href="https://twitter.com/esbeltex" target="_blank" class="text-info bg-info rounded-circle icon d-block text-white text-center mb-2"> <i class="fa fa-twitter text-white fa-2x text-center" aria-hidden="true"></i> </a>
            <hr class="bg-info rounded">
          </span>
      </div>
    </div>
  </div>
</section>
