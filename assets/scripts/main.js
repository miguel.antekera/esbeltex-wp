/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {


        (function (window, document, undefined) {
        'use strict';
          var mediaQuery = window.matchMedia('(max-width: 767px)');

          mediaQuery.addListener(doSomething);

          function doSomething(mediaQuery) {
            if (mediaQuery.matches) {
              // new WOW().init();
            } else {
              new WOW().init();
            }
          }

          doSomething(mediaQuery);

        })(window, document);



        $(window).scroll(function() {
          var scroll = $(window).scrollTop();
          if (scroll >= 160) {
              $("body").addClass("scrolled");
          } else {
              $("body").removeClass("scrolled");
          }
        });

        $('.nutrition-facts').click(function(event) {
          $('.overlay-nutrition').fadeIn('fast');
          $("html, body").animate({ scrollTop: 0 }, "slow");
          return false;
        });

        $('.rf-item > a').click(function(event) {
          return false;
        });

        $('.close-overlay, .overlay-nutrition').click(function(event) {
          $('.overlay-nutrition').fadeOut('fast');
          console.log('hola!');
          return false;
        });

        // function function_name () {
        //    $('body').addClass('slide-active scrolled');
        //  } //
        //  function_name();

        $('.show-panel').click(function() {
          $(".slide-control").show().delay(100).queue(function(){
              $(this).addClass('active').dequeue();
              $('body').addClass('slide-active');
          });
          return false;
        });

        $('.hide-panel').click(function() {
          $('.slide-control').removeClass('active');
          $('body').removeClass('slide-active');
          $(".slide-control").delay(1400).queue(function(){
              $(this).hide().dequeue();
          });
          return false;
        });

        $('.btn-step-1').click(function() {
          $('.panel-2').removeClass('hide');
          $('.panel-1').addClass('hide');
          return false;
        });

        $('.panel-3').removeClass('hide');
        $('.panel-2').addClass('hide');

        document.addEventListener( 'wpcf7mailsent', function( event ) {
            if ( '141' == event.detail.contactFormId ) {
              $('.panel-3').removeClass('hide');
              $('.panel-2').addClass('hide');
            }
        }, false );

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page

        $('.slide-home').slick({
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          autoplay: true,
          autoplaySpeed: 4000,
          dots: false,
        });

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
