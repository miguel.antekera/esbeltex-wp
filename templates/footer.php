<footer class="content-info bg-primary footer">
  <div class="container text-center">
 <a class="brand mx-auto logo-footer text-hide d-inline-block wow slideInUp" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>

  <div class="cleafix"></div>
  <p class="h3 text-white py-4 wow slideInUp">Comunícate con nosotros</p>

  <div class="clearfix"></div>




     <ul class="list-unstyled list-inline mb-0 social-icons social-icons-footer">
      <li class="list-inline-item px-3">
        <a href="https://twitter.com/esbeltex" target="_blank" class="text-white" target="_blank"> <i class="fa fa-2x fa-twitter" aria-hidden="true"></i> </a>
      </li>
      <li class="list-inline-item px-3">
        <a href="https://www.facebook.com/esbeltex" target="_blank" class="text-white" target="_blank"> <i class="fa fa-2x fa-facebook-official" aria-hidden="true"></i> </a>
      </li>
      <li class="list-inline-item px-3">
        <a href="https://www.instagram.com/esbeltex/" target="_blank" class="text-white" target="_blank"> <i class="fa fa-2x fa-instagram" aria-hidden="true"></i> </a>
      </li>
    </ul>

  <nav class="nav-footer" >
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'mb-0 py-3 list-unstyled list-inline menu-footer']);
      endif;
      ?>
    </nav>

    <p class="credits mb-0 text-white">
      El uso de este sitio web implica la aceptación de los <a href="<?php echo esc_url( home_url( '/?p=16' ) ); ?>" class="text-white">Términos y Condiciones</a> <br class="hidden-md-up hidden-sm-down"> y de las <a href="<?php echo esc_url( home_url( '/?p=16' ) ); ?>" class="text-white">Políticas de Privacidad</a> de esbeltex <br class="hidden-sm-down"/>
Copyright © <?php echo date("Y"); ?> esbeltex / Desarrollado por <a href="http://guildin.com/" class="text-white" target="_blank">GUILDIN</a>.
    </p>

    <?php ///dynamic_sidebar('sidebar-footer'); ?>
  </div>
</footer>
