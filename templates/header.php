<header class="header bg-info fixed-top">
<div class="top-bar bg-primary">
  <div class="container">
  <div class="row">
    <div class="col-6">
        <button class="navbar-toggler hidden-md-up" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"> ☰ </button>
    </div>
    <div class="col-6 text-right">
      <ul class="list-unstyled list-inline mb-0 social-icons social-icons-top d-inline-block hidden-sm-down">
      <li class="list-inline-item">
        <a href="https://www.instagram.com/esbeltex/" target="_blank" class="text-white"> <i class="fa fa-instagram" aria-hidden="true"></i> </a>
      </li>
      <li class="list-inline-item">
        <a href="https://twitter.com/esbeltex" target="_blank" class="text-white"> <i class="fa fa-twitter" aria-hidden="true"></i> </a>
      </li>
      <li class="list-inline-item">
        <a href="https://www.facebook.com/esbeltex" target="_blank" class="text-white"> <i class="fa fa-facebook-official" aria-hidden="true"></i> </a>
      </li>
    </ul>
    </div>
  </div>
  </div>
</div>
  <div class="container text-center">
    <a class="brand mx-auto logo d-block invis ible" href="<?php echo esc_url( home_url( '/' ) ); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" class="img-fluid">
    </a>
    <nav class="nav-primary bg-primary navbar-toggleable-sm rounded px-1" aria-expanded="false">
    <div class="collapse navbar-collapse" id="navbarNav">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav nav-pills nav-justified menu py-1', 'menu_id' => 'exCollapsingNavbar2']);
      endif;
      ?>
      </div>
    </nav>

  </div>
</header>
