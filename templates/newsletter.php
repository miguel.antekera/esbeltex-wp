<?php get_template_part('templates/control-peso'); ?>

<section class="subscribe bg-info py-6">
<div class="container text-center">

<h4 class="h4 text-center text-white mb-5 wow slideInUp">¿Quieres conocer más sobre el Control de Peso?
</h4>
<div class="register-newsletter">
<?php echo do_shortcode( ' [mc4wp_form id="139"] ' );; ?>
</div>
                <p class="mb-0 text-white wow slideInUp">Suscríbete a nuestro boletín para recibir promociones especiales<br class="hidden-sm-down" />
e información importante  sobre los productos esbeltex.</p>

</div>
</section>
