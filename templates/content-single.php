<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <div class="img-wrapper mb-4">
      <?php the_post_thumbnail('large', ['class' => 'img-fluid']); ?>
    </div>
    <header>
      <h1 class="entry-title fw-700 text-muted h2 mb-3"><?php the_title(); ?></h1>
      <div class="fw-400 mb-3 text-warning">
        <?php get_template_part('templates/entry-meta'); ?>
      </div>
    </header>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <footer>
      <hr>

      <div class="row">
        <div class="col col-6">
          <a class="fw-700" href="<?php echo esc_url( home_url( '/?p=8' ) ); ?>"> &laquo; Blog</a>
        </div>
        <div class="col col-6 text-right">
          <ul class="list-unstyled list-inline mb-0 text-center d-inline-block">
            <li class="list-inline-item ml-2 align-middle"> <a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><i class="fa fa-2x text-info fa-facebook-square" aria-hidden="true"></i></a> </li>
            <li class="list-inline-item ml-2 align-middle"> <a target="_blank" href="http://twitter.com/share?text=<?php the_title(); ?>&amp;url=<?php the_permalink(); ?>"><i class="fa fa-2x text-info fa-twitter-square" aria-hidden="true"></i></a> </li>
          </ul>
        </div>
      </div>

      <nav class="nav-single row mt-4 no-gutters">
        <span class="nav-previous d-inline-block fw-700 small lh-12 col col-5 left-link">
          <i class="fa fa-2x fa-angle-left"></i> <?php previous_post_link( '%link' ); ?> </span>
        <span class="col col-2"></span>
        <span class="nav-next d-inline-block fw-700 small lh-12 col col-5 text-right right-link">
          <?php next_post_link( '%link' ); ?>  <i class="fa fa-2x fa-angle-right"></i> </span>
      </nav>

    </footer>
    <?php //comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
