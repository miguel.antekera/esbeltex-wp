<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
  <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900" rel="stylesheet">

  <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon.png">
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/touch-icon-iphone-57.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/touch-icon-iphone-114.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/touch-icon-ipad-144.png" />


  <meta name="description" content="La línea de productos naturales de ESBELTEX te ayudarán a mejorar calidad de vida y tomar el control de tu peso. Empieza hoy a tener una vida saludable.">

  <?php wp_head(); ?>


<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '886473271502704');
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" src="https://www.facebook.com/tr?id=886473271502704&ev=PageView&noscript=1"/>
</noscript>


<script>
  (function(i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function() {
      (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),  m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
  ga('create', 'UA-99870393-1', 'auto');
  ga('send', 'pageview');
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.10&appId=282492851843092";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

 <script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>


<meta property="og:site_name" content="Esbeltex"/>
<meta property="og:type" content="website"/>
<meta property="fb:admins" content="268685733287986"/>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@esbeltex" />

<?php

if ( is_home() || is_front_page() ) { ?>

<meta itemprop="name" content="Mejor calidad de vida, acelera el metabolismo, más energía | Esbeltex"/>
<meta itemprop="description" content="Con Esbeltex tú puedes mejorar y tener un mayor control de tu salud mientras pierdes peso."/>
<meta itemprop="image" content="<?php echo get_template_directory_uri(); ?>/assets/images/rrss.png"/>
<meta property="og:title" content="Mejor calidad de vida, acelera el metabolismo, más energía | Esbeltex"/>
<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/assets/images/rrss.png"/>
<meta property="og:url" content="<?php echo esc_url( home_url( '/' ) ); ?>"/>
<meta property="og:description" content="Con Esbeltex tú puedes mejorar y tener un mayor control de tu salud mientras pierdes peso."/>
<meta name="twitter:title" content="Mejor calidad de vida, acelera el metabolismo, más energía | Esbeltex" />
<meta name="twitter:image" content="<?php echo get_template_directory_uri(); ?>/assets/images/rrss.png" />
<meta name="twitter:url" content="<?php echo esc_url( home_url( '/' ) ); ?>" />
<meta name="twitter:description" content="Con Esbeltex tú puedes mejorar y tener un mayor control de tu salud mientras pierdes peso." />

<?php } elseif ( is_category() ) { ?>

<?php } elseif ( is_single() ) { ?>

<?php $page_id = get_queried_object_id();
$post_thumbnail_id = get_post_thumbnail_id( $page_id );
$bkgdImg = wp_get_attachment_url( $post_thumbnail_id, 'large' );

setup_postdata( $post );
$excerpt = get_the_excerpt();

?>

<meta itemprop="name" content="Esbeltex"/>
<meta itemprop="description" content="<?php echo $excerpt ?>"/>
<meta itemprop="image" content="<?php echo $bkgdImg; ?>"/>
<meta property="og:title" content="<?php echo get_the_title(); ?>"/>
<meta property="og:image" content="<?php echo $bkgdImg; ?>" />
<meta property="og:url" content="<?php the_permalink(); ?>"/>
<meta property="og:description" content="<?php echo $excerpt ?>"/>
<meta name="twitter:title" content="<?php echo get_the_title(); ?>" />
<meta name="twitter:image" content="<?php echo $bkgdImg; ?>" />
<meta name="twitter:url" content="<?php the_permalink(); ?>" />
<meta name="twitter:description" content="<?php echo $excerpt ?>" />

<?php } elseif ( is_page() ) { ?>

<meta itemprop="name" content="Mejor calidad de vida, acelera el metabolismo, más energía | Esbeltex"/>
<meta itemprop="description" content="Con Esbeltex tú puedes mejorar y tener un mayor control de tu salud mientras pierdes peso."/>
<meta itemprop="image" content="<?php echo get_template_directory_uri(); ?>/assets/images/rrss.png"/>
<meta property="og:title" content="Mejor calidad de vida, acelera el metabolismo, más energía | Esbeltex"/>
<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/assets/images/rrss.png"/>
<meta property="og:url" content="<?php echo esc_url( home_url( '/' ) ); ?>"/>
<meta property="og:description" content="Con Esbeltex tú puedes mejorar y tener un mayor control de tu salud mientras pierdes peso."/>
<meta name="twitter:title" content="Mejor calidad de vida, acelera el metabolismo, más energía | Esbeltex" />
<meta name="twitter:image" content="<?php echo get_template_directory_uri(); ?>/assets/images/rrss.png" />
<meta name="twitter:url" content="<?php echo esc_url( home_url( '/' ) ); ?>" />
<meta name="twitter:description" content="Con Esbeltex tú puedes mejorar y tener un mayor control de tu salud mientras pierdes peso." />

<?php } else { ?>


<?php } ?>


</head>
