<div class="control-peso p-2">
  <div class="top">
  </div>

  <div class="container">
    <div class="content">
      <div class="text-center">
        <h4 class="h4 text-white empieza"><span>Empieza ahora! <a href="#" class="text-white show-panel">Únete aquí</a> al</span> <img class="img-fluid d-inline-block" src="<?php bloginfo('template_directory'); ?>/assets/images/team-esbeltex.png" /> </h4>
        <p class="mb-0 desc hidden-sm-down">Conoce todo sobre el programa natural para Control de peso.</p>
      </div>

    </div>
  </div>



</div>


<!-- slide control -->
<div class="control-peso slide-control ease hide">
  <div class="top">
     <img class="img-fluid round" src="<?php bloginfo('template_directory'); ?>/assets/images/top-peso.png" />
  </div>

  <div class="container-fluid panel-container h-100">
    <div class="content h-100">

      <a href="#" class="text-center abs-center-x hide-panel">
        <img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/peso-close.png" />
      </a>

      <!-- panel 1 -->
      <div class="row panel panel-1 h-100">
        <div class="col-4 hidden-md-down girl-wrapper">
          <div class="text-center">
            <img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/peso-girl.png" />
          </div>
        </div>
        <div class="col-12 col-lg-8">
          <div class="row">
            <div class="col-12 col-sm-6 text-center text-white">
              <h3 class="h3 mb-2 mt-4 mt-sm-6">Bienvenido al</h3>
              <img class="img-fluid mx-auto" src="<?php bloginfo('template_directory'); ?>/assets/images/peso-team.png" />
              <img class="img-fluid mx-auto mb-3" src="<?php bloginfo('template_directory'); ?>/assets/images/peso-line.png" />
              <h3 class="h3 mb-2 mb-sm-5"><span class="fw-300">Llega a tu talla ideal!</span> <br>
                con el programa natural <br>
                para Control de peso </h3>
                <div class="action">
                  <p class="mb-2">Comenzar el Programa GRATIS</p>
                  <a href="#" class="control-registro btn btn-danger px-5 btn-lg rounded mb-5 w-100 btn-step-1">Regístrate Aquí!</a>
                </div>
            </div>
            <div class="col-6 text-center hidden-xs-down">
              <img class="img-fluid mx-auto product" src="<?php bloginfo('template_directory'); ?>/assets/images/garcinia.png" />
            </div>
            <img class="img-fluid mx-auto hidden-sm-down" src="<?php bloginfo('template_directory'); ?>/assets/images/peso-cinta.png" />
          </div>
        </div>
      </div>
      <!-- .panel 1 -->

      <!-- panel 2 -->
      <div class="row panel panel-2 h-100 justify-content-center hide">
        <div class="col-4 col-sm-5 col-lg-4 girl-wrapper hidden-sm-down">
          <div class="text-center d-flex h-100">
            <div class="align-self-end">
              <h3 class="h3 mt-3 mb-3 text-white"><span class="fw-300">Llega a tu talla ideal!</span> <br>
                  con el programa natural <br>
                  para Control de peso </h3>
              <img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/peso-girl2.png" />
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-5">

             <h3 class="h4 mt-sm-5 mt-4 mb-3 text-white">
              Regístrate y Descarga GRATIS! </h3>

              <?php echo do_shortcode( '[contact-form-7 id="141" title="control peso"]' ); ?>

            <img class="img-fluid mx-auto hidden-xs-down" src="<?php bloginfo('template_directory'); ?>/assets/images/peso-cinta.png" />

        </div>
      </div>
      <!-- .panel 2 -->

      <!-- panel 3 -->
      <div class="row panel panel-3 h-100 hide">
        <div class="col-4 hidden-md-down girl-wrapper">
          <div class="text-center">
            <img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/peso-girl.png" />
          </div>
        </div>
        <div class="col-12 col-lg-8">
          <div class="row justify-content-center">
            <div class="col-12 col-sm-7 text-center text-white">
              <h3 class="h3 mb-2 mt-6">Gracias por registrarte al</h3>
              <img class="img-fluid mx-auto" src="<?php bloginfo('template_directory'); ?>/assets/images/peso-team.png" />
              <img class="img-fluid mx-auto mb-3" src="<?php bloginfo('template_directory'); ?>/assets/images/peso-line.png" />
              <p class="h6 mb-5 fw-300 lh-16">
                ¡Bienvenido al Team Esbeltex! Descarga tu guía ahora y disfruta de todo lo que necesitas saber para controlar tu peso como estilo de vida. Si deseas, puedes usar nuestro <a href="<?php echo esc_url( home_url( '/' ) ); ?>?p=10" class="text-white font-weight-bold">Localizador Esbeltex</a> para encontrar cualquier producto de Esbeltex cerca de usted y sacar el mayor provecho del programa.
               </p>
                <div class="action">
                  <a href="<?php echo esc_url( home_url( '/' ) ); ?>Esbeltex-Programa_Control_de_Peso-D1.pdf" class="control-registro btn btn-danger px-5 btn-lg rounded mb-5" download="<?php echo esc_url( home_url( '/' ) ); ?>Esbeltex-Programa_Control_de_Peso-D1.pdf" download>Descargar Ahora</a>
                </div>
            </div>
            <div class="col-5 text-center hidden-sm-down">
              <img class="img-fluid mx-auto product" src="<?php bloginfo('template_directory'); ?>/assets/images/garcinia.png" />
            </div>
            <div class="text-center mx-auto hidden-sm-down">
              <ul class="list-unstyled">
                <li class="d-inline-block px-4">
                  <a href="https://www.facebook.com/esbeltex" target="_blank">
                  <img class="img-fluid mx-auto icon-social" src="<?php bloginfo('template_directory'); ?>/assets/images/peso-fb.png" />
                  </a>
                </li>
                <li class="d-inline-block px-4">
                  <a href="https://twitter.com/esbeltex" target="_blank">
                    <img class="img-fluid mx-auto icon-social" src="<?php bloginfo('template_directory'); ?>/assets/images/peso-tw.png" />
                  </a>
                </li>
                <li class="d-inline-block px-4">
                  <a href="https://www.instagram.com/esbeltex/" target="_blank">
                    <img class="img-fluid mx-auto icon-social" src="<?php bloginfo('template_directory'); ?>/assets/images/peso-ins.png" />
                  </a>
                </li>
                <li class="d-inline-block">
                  <img class="img-fluid mx-auto icon-social" src="<?php bloginfo('template_directory'); ?>/assets/images/peso-siguenos.png" />
                </li>
              </ul>
              <img class="img-fluid mx-auto hidden-sm-down" src="<?php bloginfo('template_directory'); ?>/assets/images/peso-cinta2.png" />
            </div>
          </div>
        </div>
      </div>
      <!-- .panel 3 -->


    </div>
  </div>
</div>


