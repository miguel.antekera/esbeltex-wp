<?php

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'bones_flush_rewrite_rules' );

// Flush your rewrite rules
function bones_flush_rewrite_rules() {
	flush_rewrite_rules();
}

// let's create the function for the custom type
function custom_post_reviews() {
	// creating (registering) the custom type
	register_post_type( 'reviews', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Reviews', 'sage' ), /* This is the Title of the Group */
			'singular_name' => __( 'Review', 'sage' ), /* This is the individual type */
			'all_items' => __( 'Todos', 'sage' ), /* the all items menu item */
			'add_new' => __( 'Agregar nuevo', 'sage' ), /* The add new menu item */
			'add_new_item' => __( 'Agregar nuevo review', 'sage' ), /* Add New Display Title */
			'edit' => __( 'Editar', 'sage' ), /* Edit Dialog */
			'edit_item' => __( 'Editar review', 'sage' ), /* Edit Display Title */
			'new_item' => __( 'Nuevo review', 'sage' ), /* New Display Title */
			'view_item' => __( 'Ver review', 'sage' ), /* View Display Title */
			'search_items' => __( 'Buscar review', 'sage' ), /* Search Custom Type Title */
			'not_found' =>  __( 'No hay review agregados.', 'sage' ), /* This displays if there are no entries yet */
			'not_found_in_trash' => __( 'Vacío', 'sage' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'reviews esbeltex', 'sage' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => 'dashicons-cart', // http://calebserna.com/dashicons-cheatsheet/
			'rewrite'	=> array( 'slug' => 'reviews', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'reviews', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'author', 'thumbnail', 'revisions', 'editor')
		) /* end of options */
	); /* end of register post type */

	/* this adds your post categories to your custom post type */
  // register_taxonomy_for_object_type( 'category', 'reviews' );
	/* this adds your post tags to your custom post type */
	// register_taxonomy_for_object_type( 'post_tag', 'reviews' );

}

	// adding the function to the Wordpress init
	add_action( 'init', 'custom_post_reviews');

	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/

	// now let's add custom categories (these act like categories)
	register_taxonomy( 'custom_cat_review',
		array('reviews'), /* if you change the name of register_post_type( 'reviews', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Categoría', 'sage' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Categoría', 'sage' ), /* single taxonomy name */
				'search_items' =>  __( 'Buscar Categoría', 'sage' ), /* search title for taxomony */
				'all_items' => __( 'Todas', 'sage' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Custom Category', 'sage' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Custom Category:', 'sage' ), /* parent taxonomy title */
				'edit_item' => __( 'Editar', 'sage' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update', 'sage' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New', 'sage' ), /* add new title for taxonomy */
				'new_item_name' => __( 'Nueva categoría', 'sage' ) /* name title for taxonomy */
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'categoria-review' ),
		)
	);

	// now let's add custom tags (these act like categories)
	// register_taxonomy( 'custom_tag',
	// 	array('reviews'), /* if you change the name of register_post_type( 'reviews', then you have to change this */
	// 	array('hierarchical' => false,    /* if this is false, it acts like tags */
	// 		'labels' => array(
	// 			'name' => __( 'Custom Tags', 'sage' ), /* name of the custom taxonomy */
	// 			'singular_name' => __( 'Custom Tag', 'sage' ), /* single taxonomy name */
	// 			'search_items' =>  __( 'Search Custom Tags', 'sage' ), /* search title for taxomony */
	// 			'all_items' => __( 'All Custom Tags', 'sage' ), /* all title for taxonomies */
	// 			'parent_item' => __( 'Parent Custom Tag', 'sage' ), /* parent title for taxonomy */
	// 			'parent_item_colon' => __( 'Parent Custom Tag:', 'sage' ), /* parent taxonomy title */
	// 			'edit_item' => __( 'Edit Custom Tag', 'sage' ), /* edit custom taxonomy title */
	// 			'update_item' => __( 'Update Custom Tag', 'sage' ), /* update title for taxonomy */
	// 			'add_new_item' => __( 'Add New Custom Tag', 'sage' ), /* add new title for taxonomy */
	// 			'new_item_name' => __( 'New Custom Tag Name', 'sage' ) /* name title for taxonomy */
	// 		),
	// 		'show_admin_column' => true,
	// 		'show_ui' => true,
	// 		'query_var' => true,
	// 	)
	// );

// let's create the function for the custom type
function custom_post_stores() {
  // creating (registering) the custom type
  register_post_type( 'stores', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
    // let's now add all the options for this post type
    array( 'labels' => array(
      'name' => __( 'Tiendas', 'sage' ), /* This is the Title of the Group */
      'singular_name' => __( 'Tiendas', 'sage' ), /* This is the individual type */
      'all_items' => __( 'Todas', 'sage' ), /* the all items menu item */
      'add_new' => __( 'Agregar nueva', 'sage' ), /* The add new menu item */
      'add_new_item' => __( 'Agregar nueva review', 'sage' ), /* Add New Display Title */
      'edit' => __( 'Editar', 'sage' ), /* Edit Dialog */
      'edit_item' => __( 'Editar tienda', 'sage' ), /* Edit Display Title */
      'new_item' => __( 'Nueva tienda', 'sage' ), /* New Display Title */
      'view_item' => __( 'Ver tienda', 'sage' ), /* View Display Title */
      'search_items' => __( 'Buscar tienda', 'sage' ), /* Search Custom Type Title */
      'not_found' =>  __( 'No hay tienda agregados.', 'sage' ), /* This displays if there are no entries yet */
      'not_found_in_trash' => __( 'Vacío', 'sage' ), /* This displays if there is nothing in the trash */
      'parent_item_colon' => ''
      ), /* end of arrays */
      'description' => __( 'Tiendas esbeltex', 'sage' ), /* Custom Type Description */
      'public' => true,
      'publicly_queryable' => false,
      'exclude_from_search' => true,
      'show_in_rest'       => true,
      // 'rest_base'          => 'books-api',
      // 'rest_controller_class' => 'WP_REST_Posts_Controller',
      'show_ui' => true,
      'query_var' => true,
      'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
      'menu_icon' => 'dashicons-admin-multisite', // http://calebserna.com/dashicons-cheatsheet/
      'rewrite'  => array( 'slug' => 'tiendas', 'with_front' => false ), /* you can specify its url slug */
      'has_archive' => 'stores', /* you can rename the slug here */
      'capability_type' => 'post',
      'hierarchical' => false,
      /* the next one is important, it tells what's enabled in the post editor */
      'supports' => array( 'title', 'author', 'revisions', 'editor')
    ) /* end of options */
  ); /* end of register post type */

  /* this adds your post categories to your custom post type */
  // register_taxonomy_for_object_type( 'category', 'stores' );
  /* this adds your post tags to your custom post type */
  // register_taxonomy_for_object_type( 'post_tag', 'stores' );

}

  // adding the function to the Wordpress init
  add_action( 'init', 'custom_post_stores');




?>
