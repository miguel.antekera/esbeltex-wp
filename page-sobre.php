<?php
/**
 * Template Name: Sobre nosotros
 */
?>

<section class="bg-faded">
  <div class="page-header page-header-nosotros text-center text-primary pt-6">
    <h1 class="fw-300 mx-auto h2 mb-0 mt-4 title">
      La calidad de los productos esbeltex  <br />
      está garantizada gracias a la experiencia <br />
      <span class="fw-900">y trayectoria exitosa de Medix.</span>
    </h1>
  </div>
</section>

<section class="banner-sobre">
  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/medix.jpg" class="img-fluid">
</section>

<section class="content-sobre bg-primary text-white skew-primary">
  <div class="container wow fadeInUp">
    <div class="row">
      <div class="col-md-8 push-md-2">
        <!-- <h1 class="title title-underline h2 mb-5 pb-3 d-inline-block">Mision y visión</h1> -->
    <h3 class="fw-300 lh-12 mb-5">SALULLEZA, forma parte de la gran familia Medix, una empresa internacional especializada en el control de sobrepeso y obesidad.</h3>
<p class="fw-300 h5 lh-14 mb-5">A través de los años, hemos alcanzado el liderazgo en México, Centroamérica y el Caribe en los tratamientos del sobrepeso y obesidad, y por nuestra búsqueda constante de soluciones integrales, atribuyendo a la mejora de calidad de vida, con los cuales contribuimos a disminuir el impacto de diferentes alteraciones metabólicas.
</p>
      </div>
    </div>
  </div>
</section>

<section class="bg-faded pb-6">
  <div class="container wow fadeInUp">
    <div class="row">
      <div class="col-md-8 push-md-2">
      <br />
        <h1 class="title title-underline-primary h2 mb-5 pb-3 d-inline-block text-primary mt-6 pt-6">¿Quiénes Somos?</h1>
    <h3 class="fw-300 lh-12 mb-5 text-primary">Somos el laboratorio farmacéutico con el mayor portafolio
de medicamentos para el tratamiento de sobrepeso y
obesidad a nivel mundial.  </h3>

<p class="fw-300 h5 lh-14 mb-3">
Hemos trabajado por más de 60 años en el desarrollo de productos que mejoren la calidad de vida de la gente alrededor del mundo.
</p>
<p class="fw-300 h5 lh-14 mb-3">
En Medix® tenemos una variedad de productos seguros y efectivos que proveen soluciones integrales para pacientes con deficiencias metabólicas.
 </p>


<img src="<?php echo get_template_directory_uri(); ?>/assets/images/sobre.png" class="img-fluid mb-5">

<h3 class="fw-300 lh-12 mb-5 text-primary">
Nuestra presencia abarca Centroamérica, el caribe, Estados Unidos, Brasil, Argentina y España. </h3>

<img src="<?php echo get_template_directory_uri(); ?>/assets/images/mapa.png" class="img-fluid mb-5">

  <p class="fw-300 h5 lh-14 mb-3 mb-5"> ¡Tener un peso saludable es posible! Nuestros productos combinados con una sana alimentación y ejercicio
te ayudarán a logarlo.
  </p>


      </div>
    </div>
  </div>
</section>
