<?php
/**
 * Template Name: Producto Colon
 */
?>

<section class="cover-product cover-colon relative bg-faded">
<div class="overlay-nutrition">
  <div class="container">
  <div class="tabla absolute abs-center-x">
    <a href="#" class="close-overlay text-center py-1 text-white fw-400 bg-info rounded-circle float-right mt-4 h6 small absolute">X</a>
    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/nutrition-facts-colon.png" class="img-fluid">
  </div>
  </div>
</div>
<div class="container">
  <div class="row wow fadeInDown" data-wow-delay="0.5s">

    <div class="col-12 my-auto text-white item">
      <div class="row">

        <div class="col-md-6 col-lg-5 col-xl-4 push-lg-7 push-xl-7 push-md-6 mb-5 mb-sm-0">
          <h1 class="fw-300 mb-0 title relative text-blue-dark h1">
          esbeltex <span class="fw-900"> Colon </span> <br />
          <span class="fw-900"> Cleanser Detox </span>
          </h1>
          <hr class="bg-info mt-2">
          <ul class="mb-0 list-inline list-unstlyed icons-products py-4 mb-2 d-flex">
            <li class="list-inline-item pr-4">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/icon-colon1.png" class="img-fluid">
            </li>
            <li class="list-inline-item px-4">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/icon-colon2.png" class="img-fluid">
            </li>
            <li class="list-inline-item px-4">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/icon-colon3.png" class="img-fluid">
            </li>
          </ul>

          <div class="hidden-sm-down">
            <p class="text-gray-dark fw-400 small"> Es un complemento que te ayudará a mejorar el tránsito intestinal, previene la inflamación y el estreñimiento. Manten el vientre plano con esbeltex colon cleanser detox. </p>
            <a href="#" class="btn btn-primary rounded btn-sm px-3 text-white nutrition-facts"> Nutrition Facts</a>
          </div>
        </div>

        <div class="col-md-5 col-lg-4 col-xl-3 pull-md-5 pull-lg-2 pull-xl-0 my-auto text-center">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/colon.png" class="img-fluid">
          <div class="hidden-md-up texto-product">
            <p class="text-gray-dark fw-400 small">Es un complemento que te ayudará a mejorar el tránsito intestinal, previene la inflamación y el estreñimiento. Manten el vientre plano con esbeltex colon cleanser detox. </p>
            <a href="#" class="btn btn-primary rounded btn-sm px-3 text-white nutrition-facts"> Nutrition Facts</a>
          </div>
        </div>

      </div>
    </div>


  </div>
</div>

</section>


<section class="benefits-products py-6 skew-bg">
  <div class="container my-0 my-md-6">
   <img src="<?php echo get_template_directory_uri(); ?>/assets/images/circle-detox.png" class="text-bg abs-center img-fluid wow fadeInUp hidden-xs-down">
  <div class="row">
    <div class="col-sm-6 offset-sm-3 text-center text-white py-2">
      <h2 class="title h2 text-white wow fadeInUp" data-wow-delay="0.3s">
        esbeltex <br />
Colon Cleanser Detox
      </h2>
      <p class="mb-0 lead wow fadeInUp" data-wow-delay="0.8s">Colon Cleanse detox es una mezcla de hierbas que trabajan en conjunto para ayudarte a cuidar de tu colon evitando problemas gastrointestinales. </p>
    </div>
  </div>
  </div>
</section>


<section class="ingredientes py-2">
  <div class="container">



  <div class="section-title wow slideInUp mx-auto text-center mb-5">
    <h2 class="h1 red text-uppercase text-turquesa fw-900 mt-0">INGREDIENTES</h2>
  <p>Es el complemento ideal para ti que has decido <br class="hidden-xs-down"> tomar el control y tener una vida más saludable.</p>
  <hr class="divider bg-turquesa rounded mx-auto">
  </div>


  <div class="text-center py-4">
    <ul class="icons-products list-inline list-unstlyed d-inline-flex mb-5 wow fadeInUp">
      <li class="list-inline-item align-self-xl-stretch px-3 px-md-6">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/ingcc1.png" class="img-fluid">
      </li>
      <li class="list-inline-item align-self-xl-stretch px-3 px-md-6">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/ingcc2.png" class="img-fluid">
      </li>
      <li class="list-inline-item align-self-xl-stretch px-3 px-md-6">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/ingcc3.png" class="img-fluid">
      </li>
      </ul>
      <ul class="icons-products list-inline list-unstlyed d-inline-flex mb-5 wow fadeInUp">
      <li class="list-inline-item align-self-xl-stretch px-3 px-md-6">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/ingcc4.png" class="img-fluid">
      </li>
      <li class="list-inline-item align-self-xl-stretch px-3 px-md-6">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/ingcc5.png" class="img-fluid">
      </li>
      <li class="list-inline-item align-self-xl-stretch px-3 px-md-6">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/ingcc6.png" class="img-fluid">
      </li>
    </ul>
          <div class="clearfix"></div>
           <p class="py-3 wow fadeInUp" data-wow-delay="0.2s">Otros Ingredientes: <br>
Super mezcla de limpieza, Psyllium, cascara sagrada, raíz de riubarbo, raíz de regaliz,<br> hojas de senna, corteza de Espino serval.</p>
  </div>

  </div>
</section>


<section class="beneficios bg-faded">

<div class="container">

<div class="section-title wow slideInUp mx-auto text-center mb-5">
    <h2 class="h1 red text-uppercase text-primary fw-900 pt-3 wow fadeInUp">Beneficios</h2>
  <p class="wow fadeInUp">
  Mejora el tránsito intestinal, evita el estreñimiento, siente ligereza.</p>
  <hr class="divider bg-primary rounded mx-auto">
  </div>


  <div class="clearfix">
    <div class="col-xs-12">

    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/cl-girl-beneficios.png" class="img-fluid absolute woman-beneficios">

    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/cl-men-beneficios.png" class="img-fluid absolute men-beneficios">

<img src="<?php echo get_template_directory_uri(); ?>/assets/images/beneficios-colon.png" class="img-fluid mx-auto d-block wow zoomIn hidden-sm-down" data-wow-delay="0.5s">




<div class="hidden-md-up text-center px-4">

      <h4 class="h4 fw-400 text-primary wow fadeInUp bottom-text lh-12 my-6">Mantener un colon saludable te ayudará a que los demás órganos se mantengan saludables, evitando el acumulo de toxinas en el organismo.</h4>

      <span class="sep-dashed text-turquesa"></span>
      <h4 class="text-turquesa">● Evita la inflamación abdominal  </h4>
<p class="fw-300 h5 lh-14 mb-6">Te ayuda a mantener el vientre plano. </p>

<span class="sep-dashed text-turquesa"></span>
<h4 class="text-turquesa">● Previene el estreñimiento </h4>
<p class="fw-300 h5 lh-14 mb-6">Ingredientes como el psyllium te ayudaran a mejorar el tránsito intestinal, gracias a la gran cantidad de fibra que te proporciona.
</p>

<span class="sep-dashed text-turquesa"></span>
<h4 class="text-turquesa">● 100% natural  </h4>
<p class="fw-300 h5 lh-14 mb-6">Psyllium, cascara sagrada, raíz de riubarbo, raíz de regaliz, hojas de senna, corteza de Espino serval  entre otras, son solo algunos de los ingredientes naturales que tiene.
</p>

<span class="sep-dashed text-turquesa"></span>
<h4 class="text-turquesa">● Ayuda en la eliminación de toxinas  </h4>
<p class="fw-300 h5 lh-14 mb-6">Acumuladas en el colon para evitar enferemedades relacionadas.
</p>

<span class="sep-dashed text-turquesa"></span>
<h4 class="text-turquesa">● SafeProduct  </h4>
<p class="fw-300 h5 lh-14 mb-6">Sin contraindicaciones al usar el producto
</p>

<span class="sep-dashed text-turquesa"></span>
<h4 class="text-turquesa">● Resultados asegurados  </h4>
<p class="fw-300 h5 lh-14 mb-6">Experimenta la sensación de ligereza esbeltex.
</p>

    </div>


<div class="row">
  <div class="col-md-6 push-md-3 col-lg-4 push-lg-4 text-center py-6">
    <p class="h4 fw-400 text-turquesa wow fadeInUp bottom-text lh-14">Adquiere hoy esbeltex colon cleanser detox, mejora tu digestión, siente ligereza y mantén un abdomen plano.</p>
<a href="<?= esc_url(home_url('/')); ?>?p=10" class="btn text-white bg-orange rounded px-3 mb-6 mt-6 wow fadeInUp">Donde Comprar</a>
  </div>
</div>
    </div>
  </div>
</div>

</section>


<section class="featured py-2 bg-turquesa">

<div class="container">

  <div class="text-center">
    <ul class="mb-0 list-inline list-unstlyed py-3 d-sm-inline-flex d-block wow fadeInUp">
            <li class="d-block d-sm-inline-block px-2 px-lg-4 align-middle">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/cl-feat-1.png" class="img-fluid mb-1 ml-5 ml-sm-0">
              <p class="small text-white mr-sm-5">esbeltex contiene menos <br /> 20 ppm de gluten
</p>
            </li>
            <li class="d-block d-sm-inline-block px-2 px-lg-4 align-middle">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/cl-feat-2.png" class="img-fluid mb-1">
               <p class="small text-white">esbeltex no ha sido modificado<br /> genéticamente para alterar<br /> su propiedades
</p>
            </li>
          </ul>
  </div>

  <!-- <div class="row">
    <div class="push-sm-2 col-sm-8">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/3_hSS2NSBnY" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>
  </div> -->

</div>

</section>

<section class="reviews-wrapper py-6">

<div class="container">
  <div class="row">
    <div class="col-10 push-1">
      <div id="accordion" role="tablist" aria-multiselectable="true">



 <?php
 global $post;
 $i=0;
      $loop = new WP_Query( array(
        'showposts' => 100,
        'post_type' => array('reviews'),
        'orderby' => 'rand',
        'tax_query' => array(
            array(
              'taxonomy' => 'custom_cat_review',
              'field'    => 'slug',
              'terms'    => 'colon',
            ),
          )
        )
      );
    while ( $loop->have_posts() ) : $loop->the_post(); ?>

    <?php $rating = get_field('rating'); ?>

   <div class="card mb-3" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $post->ID ?>" aria-expanded="true" aria-controls="collapse-<?php echo $post->ID ?>">
    <div class="card-header" role="tab" id="heading-<?php echo $post->ID ?>">
      <h5 class="mb-0">
          <div class="row">
            <div class="col-2 bg-rating hidden-xs-down">
              <div class="px-md-2 py-4 mt-2">
               <?php if ( has_post_thumbnail() ) : ?>
                  <?php the_post_thumbnail('thumbnail', ['class' => 'img-fluid rounded-circle']); ?>
                  <?php else : ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/avatar-rv.png" class="img-fluid">
                <?php endif; ?>
              </div>
            </div>
            <div class="col-12 col-sm-10 bg-rating">
              <div class="px-2 py-4 mb-2 text-white">
              <ul class="rating rating-<?php echo $rating; ?> list-inline list-unstyled">
              <li class="star list-inline-item">1</li>
              <li class="star list-inline-item">2</li>
              <li class="star list-inline-item">3</li>
              <li class="star list-inline-item">4</li>
              <li class="star list-inline-item">5</li>
            </ul>
               <h3 class="h4 fw-900">“<?php the_title(); ?>”</h3>
               <?php if(get_field('extracto')) { ?>
                 <p class="mb-1 h6"> <?php the_field('extracto') ?> </p>
               <?php } ?>
               <?php if(get_field('autor')) { ?>
                 <p class="mb-0 h6"> <?php the_field('autor') ?> </p>
               <?php } ?>
            </div>
          </div>
        </div>
      </h5>
    </div>
    <div id="collapse-<?php echo $post->ID ?>" class="collapse <?php echo ($i==0)?'show':''; ?>" role="tabpanel" aria-labelledby="heading-<?php echo $post->ID ?>">
      <div class="card-block">
        <div class="row">
          <div class="push-sm-2 col-sm-10">
            <div class="py-4 px-sm-2">
              <?php the_content(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

   <?php  $i++; endwhile; ?>
   <?php wp_reset_query(); ?>


      </div>
    </div>
  </div>
</div>

</section>
