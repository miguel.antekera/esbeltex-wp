<?php
/**
 * Template Name: Home
 */
?>

<section class="cover background-cover relative">
<div class="container vh-100">
  <div class="row slide-hom wow fadeInDown slide vh-100" data-wow-delay="0.5s">

    <div class="col-12 my-auto text-white item">
      <div class="row">
        <div class="col-md-6 push-md-6 my-auto text-right">
          <div class="content px-2 px-sm-0">
            <p class="pb-2 mb-3 sumario relative line">Toma el control de tu peso</p>
            <h1 class="fw-300 mb-0 title relative pb-5 pb-sm-6">Reduce medidas de forma natural con <strong class="fw-900 line pb-2">esbeltex <span class="trademark d-inline-block absolute">TM</span> </strong> </h1>
          </div>
        </div>
        <div class="col-md-6 pull-md-6 my-auto text-center">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/banner-1.png" class="img-fluid px-4 px-sm-0">
      </div>
      </div>
    </div>


  </div>
</div>

</section>

<?php get_template_part('templates/control-peso'); ?>


<section class="productos relative">
<div class="container">
  <div class="section-title wow slideInUp mx-auto text-center">
    <h2 class="h1 red text-uppercase text-danger fw-900 mt-0">NUESTROS PRODUCTOS</h2>
  <p class="fw-400">El complemento ideal para mejorar tu salud</p>
  <hr class="divider bg-danger rounded mx-auto">
  </div>
  <div class="row">

    <div class="col-lg-6">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/garcinia.png" class="img-fluid product-mobile d-block hidden-md-up">
      <div class="product-item wow fadeInLeftBig product-item-first float-md-left">
       <img src="<?php echo get_template_directory_uri(); ?>/assets/images/garcinia.png" class="img-fluid product absolute hidden-sm-down">
        <div class="text text-center text-md-right text-white">
        <h3 class="title h4 fw-400 h3">
          esbeltex<br>Garcinia cambogia
        </h3>
        <hr class="bg-warning my-2">
          <p>Es el complemento que te ayudará mejorar tu salud, pues sus componentes principales, estimulan la oxidación de la grasa y controlan la ingesta de alimentos, en conjunto con una buena alimentación y ejercicio
</p>
<a href="<?php echo esc_url( get_page_link( 49 ) ); ?>" class="btn rounded btn-warning">Ver más</a>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/colon.png" class="img-fluid product-mobile d-block hidden-md-up">
      <div class="product-item wow fadeInRightBig product-item-second float-md-right">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/colon.png" class="img-fluid product absolute hidden-sm-down">
        <div class="text text-center text-md-left text-white">
        <h3 class="title h4 fw-400 h3">
          esbeltex<br>Colon Cleanser Detox
        </h3>
         <hr class="bg-secondary my-2">
          <p>Es el complemento ideal que, junto al esbeltex con extracto de Garcinia cambogia, te ayudará a tener una digestión saludable porque ayuda a la desintoxicación del cuerpo, previene el estreñimiento, acelera el metabolismo y te da más energía para enfrentar tu día
</p>
<a href="<?php echo esc_url( get_page_link( 51 ) ); ?>" class="btn rounded btn-secondary">Ver más</a>
        </div>
      </div>
    </div>
  </div>
</div>
</section>


<section class="comunidad relative">
<img src="<?php echo get_template_directory_uri(); ?>/assets/images/camino.png" class="img-fluid girl-running">
<div class="container">
<img src="<?php echo get_template_directory_uri(); ?>/assets/images/pesas.png" class="pesas absolute icons" id="pesas">
<img src="<?php echo get_template_directory_uri(); ?>/assets/images/fruits.png" class="fruits absolute icons" id="fruits">

  <div class="section-title wow slideInUp mx-auto text-center">
    <h2 class="h2 red text-uppercase text-primary fw-900 mt-0">COMUNIDAD esbeltex</h2>

  <p>Tener un vida más saludable puede ser muy fácil <br />
si sigues estos consejos que tenemos para ti</p>
   <hr class="divider bg-primary rounded mx-auto">
  </div>

  <div class="row list-unstyled list-inline blog w-100 text-center py-6 text-center">


    <?php
    $temp = $wp_query; $wp_query= null;
    $wp_query = new WP_Query(); $wp_query->query('posts_per_page=5' . '&paged='.$paged);
    while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

    <div class="item list-inline-item relative m-0 w-20 px-2 text-center wow fadeInUp mb-4 mb-md-0">
      <a href="<?php the_permalink(); ?>">
        <div class="img-wrapper overflow-hidden">
          <?php if ( has_post_thumbnail() ) {
          the_post_thumbnail('post-img-home', array( 'class'  => 'img-fluid' ));
          } else { ?>
          <img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/no-image-post-home.png"
          alt="<?php the_title(); ?>" />
          <?php } ?>
        </div>
        <h3 class="h6 my-4 title"> <a href="<?php the_permalink(); ?>" class="text-muted"> <?php the_title(); ?> </h3>
        <span class="btn btn-link ease abs-center-x">Ver más</span>
        </a>
      </a>
    </div>

    <?php endwhile; ?>

    <?php wp_reset_postdata(); ?>

  </div>
</div>
</section>


<section class="comprar relative">
<div class="container">
<img src="<?php echo get_template_directory_uri(); ?>/assets/images/model.png" class="model absolute">
  <div class="section-title wow slideInUp mx-auto text-center">
    <h2 class="h2 red text-uppercase text-gray-dark fw-900 mt-0">DÓNDE COMPRAR</h2>
  <p>El control y el manejo de tu salud está más cerca de lo que piensas. <br />
Consigue aquí esbeltex </p>
 <hr class="divider bg-gray-dark rounded">
  </div>

<div class="row">
<div class="col-md-6 offset-md-3 text-center postal-code">
<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/location.png" class="mx-auto d-inline-block mb-4 wow bounce" data-wow-iteration="infinite" data-wow-duration="2s">
  <div class="form-group">
  </div>
  <a href="<?php echo esc_url( home_url( '/donde-comprar' ) ); ?>" class="btn btn-danger mx-auto rounded py-2">Buscar tienda más cercana</a>
</div>
</div>


</div>
</section>
