<?php
/**
 * Template Name: Donde comprar
 */
?>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBgyq_LHJJM0Xk9aLeX3bI7me-ab7MfVs8&amp;libraries=places"></script>


<div class="bg-faded pt-6">
  <div class="page-header text-center wow fadeInUp">
    <h1 class="fw-300 mx-auto h2 mt-2 text-danger mb-0">
      El control y el manejo de tu salud  <br class="hidden-sm-down" />
      está más cerca de lo que piensas.  <br />
      <span class="fw-900">Consigue esbeltex en tu tienda más cercana</span>
    </h1>
    <p class="pt-3 pb-1">Ingresa aquí tu ciudad o tu código postal para que sepas <br>
dónde queda tu tienda más cercana a tu ubicación.</p>
  </div>

  <div class="container text-center wow fadeInUp pb-5 subscribe">
    <div class="wow slideInUp d-block">
     <ul class="mb-0 list-unstyled">
       <li class="d-inline-block register-newsletter location-form">
          <div class="relative">
            <input type="text" id="location" placeholder="Ciudad o código postal" class="form-control d-inline-block rounded form-control-static py-2 px-4 mb-3 mb-md-0  mr-2 deletable">
          <i class="fa fa-times reset reset-search" aria-hidden="true"></i>
          </div>
       </li>
       <li class="d-inline-block hidden-xs-down px-2">ó</li>
       <li class="d-inline-block">
          <span class="input-group-addon btn btn-info rounded py-2 geolocation">
            <i class="fa fa-fw fa-spinner fa-spin animated mr-1"></i>
            <i class="fa fa-fw fa-map-marker mr-1"></i>
            Encuentrame
          </span>
       </li>
     </ul>
    </div>
    <ul class="list-unstyled mb-0 products-where">
      <li class="d-inline-block">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/garcinia.png" class="img-fluid">
      </li>
      <li class="d-inline-block">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/colon.png" class="img-fluid">
      </li>
    </ul>
  </div>


  <div class="map-canvas row no-gutters" id="map-canvas">
      <div class="col-12 col-sm-3 map-side">
          <div class="items-list">
              <div class="inner">
                  <header>
                      <h3 class="h6 text-blue-dark fw-700">Tiendas cercanas</h3>
                  </header>
                  <ul class="results list">
                  </ul>
              </div>
          </div>
      </div>
      <!-- listado -->
      <div class="col-12 col-sm-9 map-col">
          <div class="map">
              <div class="toggle-navigation hidden-sm-down">
                  <div class="icon">
                      <div class="line"></div>
                      <div class="line"></div>
                      <div class="line"></div>
                  </div>
              </div>
              <!-- toggle -->
          </div>
          <div id="map"></div>
          <!-- #map -->
      </div>
      <!-- .col -->
  </div>
  <!-- .map-canvas -->


</div>
