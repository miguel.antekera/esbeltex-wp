<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php', // Theme customizer

  // 'lib/bs4navwalker.php', // Menu bootstrap 4

  // I like vuelticas
  'lib/steroids/custom-post-type.php',
  'lib/steroids/admin.php',

  // Soil Plugins Functions
  'lib/steroids/modules/clean-up.php',
  'lib/steroids/modules/disable-asset-versioning.php',
  'lib/steroids/modules/disable-trackbacks.php',
  'lib/steroids/modules/jquery-cdn.php',
  'lib/steroids/modules/js-to-footer.php',
  'lib/steroids/modules/nice-search.php',
];


foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

function my_acf_google_map_api( $api ){
  $api['key'] = 'AIzaSyBgyq_LHJJM0Xk9aLeX3bI7me-ab7MfVs8';
  return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');


// Return all post IDs
function get_all_post( $data ) {
  $posts = get_posts([
    'post_type'         => 'stores',
    'post_status'       => 'publish',
    'posts_per_page'    => 100,
  ]);

  if (empty( $posts ) ) {
    return null;
  }

  $data = [];

  foreach ($posts as $post) {

    $nombre_tienda = get_field('nombre_tienda', $post->ID, 'format_value');
    $latitud = get_field('latitud', $post->ID, 'format_value');
    $longitud = get_field('longitud', $post->ID, 'format_value');
    $productos = get_field('productos', $post->ID, 'format_value');
    $address = get_field('map_location', $post->ID, 'format_value');

    $api_content = [
        'id'  => $post->ID,
        'name'  => $nombre_tienda,
        'map_location' => $address,
        'lat' => $latitud,
        'long' => $longitud,
        'products' => $productos,
    ];
    $data[] = $api_content;
  }

   // cache for 2 hours
    set_transient( 'dt_all_post_ids', $data, 60*60*2 );

    return $data;
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'storesapi/v1', '/get-all-posts/', array(
    'methods' => 'GET',
    'callback' => 'get_all_post',
  ) );
} );

function myJson() {


  $url = "http://dev.esbeltex.com/wp-json/storesapi/v1/get-all-posts";

  $request = new WP_Http;
  $result = $request->request($url, $data = array());

  if (is_array($result) && !empty($result['body'])) {
     $data = json_encode($result['body']);
     $array = json_decode($data, true);
     return $array;
  } else {
     return false;
  }

  print_r($array);

}
