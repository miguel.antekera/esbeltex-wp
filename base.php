<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>

     <?php  if (is_page(10)) { ?>
        <div id="preloader">
          <div id="status">&nbsp;</div>
        </div>
      <?php } ?>

    <!--[if IE]>
      <div class="alert alert-warning mb-0">
        <?php _e('Estas utilizando un navegador <strong>antiguo</strong>. Por favor <a href="http://browsehappy.com/">actualiza tu navegador</a> para mejorar tu experiencia.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
        get_template_part('templates/header');
    ?>
    <div class="wrap" role="document">
          <?php include Wrapper\template_path(); ?>
    </div><!-- /.wrap -->
    <?php   get_template_part('templates/newsletter'); ?>
    <?php
        do_action('get_footer');
        get_template_part('templates/footer');
      wp_footer();
    ?>

    <?php  if (is_page(10)) { ?>

      <script>

        var _latitude = 25.7616798;
        var _longitude = -80.1917902;
        var jsonPath = <?php echo myJson(); ?>

        function initialize (_latitude,_longitude, jsonPath) {

            $('.geolocation').removeClass('loading');
            createHomepageGoogleMap(_latitude,_longitude,jsonPath);
            $('#preloader').hide();
        }

        $(window).load(function(){
            initialize(_latitude, _longitude, jsonPath);
        });

        autoComplete();
      </script>

    <?php } ?>
  </body>
</html>
